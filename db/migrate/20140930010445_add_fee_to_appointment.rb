class AddFeeToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :fee, :float
  end
end
