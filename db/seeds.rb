# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Insurance.create(name: 'Aetna', street_address: '151 Farmington Avenue Hartford, CT 06156')
Insurance.create(name: 'United Healthcare', street_address: '1250 Capital of TX Hwy South, Bldg. 1 Ste 400 , Austin , TX 7874')
Insurance.create(name: 'Humana', street_address: '500 West Main Street Louisville, KY 40202 ')
Insurance.create(name: 'BlueCross BlueShield', street_address: '225 North Michigan Avenue Chicago, IL 60601')
Insurance.create(name: 'ObamaCare', street_address: ' 1600 Pennsylvania Ave NW, Washington, DC 20500 ')

Patient.create(name: 'Saad Khan', street_address: '4401 Wheeler St', name: 'Aetna' )
Patient.create(name: 'Sundas Ahmad', street_address: '122344 Armtriage Ln', name:'Humana' )
Patient.create(name: 'Amanda Ripley', street_address: ' USS Sulaco ', name:'Obamacare' )
Patient.create(name: 'Kyle Manori ', street_address: '23454 Wind Trace Cv', name:'BlueCross BlueShield' )
Patient.create(name: 'Kim Kardashian', street_address: 'California', name:'Obamacare' )